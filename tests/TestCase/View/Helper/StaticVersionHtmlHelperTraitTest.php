<?php

namespace Chris48cf\StaticVersionHelper\Test\TestCase\View\Helper;

use Cake\Cache\Cache;
use Cake\TestSuite\TestCase;
use Chris48cf\StaticVersionHelper\Test\TestCase\View\Helper\HtmlHelperStub;

class SearchableBehaviorTest extends TestCase
{

    private $helper;
    private $expectedHash;

    public function setUp()
    {
        parent::setUp();
        Cache::clear(false, 'default');
        $this->helper = new HtmlHelperStub(new \Cake\View\View());
        $this->expectedHash = md5($this->helper->fakeDirList);
    }

    public function tearDown()
    {
        unset($this->helper);
        Cache::clear(false, 'default');
        parent::tearDown();
    }

    public function testStringCssWithVersion()
    {
        $result = $this->helper->css('foo.css', ['versioned' => true]);
        $this->assertEquals('<link rel="stylesheet" href="/foo.css?hash=' . $this->expectedHash . '"/>', $result);
    }

    public function testArrayCssWithVersion()
    {
        $result = $this->helper->css(['foo.css', 'bar.css'], ['versioned' => true]);
        $this->assertNotEquals(
            false,
            strpos($result, '<link rel="stylesheet" href="/foo.css?hash=' . $this->expectedHash . '"/>')
        );
        $this->assertNotEquals(
            false,
            strpos($result, '<link rel="stylesheet" href="/bar.css?hash=' . $this->expectedHash . '"/>')
        );
    }

    public function testStringCssNoVersion()
    {
        $result = $this->helper->css('foo.css');
        $this->assertEquals('<link rel="stylesheet" href="/foo.css"/>', $result);
    }

    public function testArrayCssNoVersion()
    {
        $result = $this->helper->css(['foo.css', 'bar.css'], ['versioned' => false]);
        $this->assertNotEquals(
            false,
            strpos($result, '<link rel="stylesheet" href="/foo.css"/>')
        );
        $this->assertNotEquals(
            false,
            strpos($result, '<link rel="stylesheet" href="/bar.css"/>')
        );
    }

    public function testStringScriptWithVersion()
    {
        $result = $this->helper->script('foo.js', ['versioned' => true]);
        $this->assertEquals('<script src="/foo.js?hash=' . $this->expectedHash . '"></script>', $result);
    }

    public function testArrayScriptWithVersion()
    {
        $result = $this->helper->script(['foo.js', 'bar.js'], ['versioned' => true]);
        $this->assertNotEquals(
            false,
            strpos($result, '<script src="/foo.js?hash=' . $this->expectedHash . '"></script>')
        );
        $this->assertNotEquals(
            false,
            strpos($result, '<script src="/bar.js?hash=' . $this->expectedHash . '"></script>')
        );
    }

    public function testStringScriptNoVersion()
    {
        $result = $this->helper->script('foo.js', ['versioned' => false]);
        $this->assertEquals('<script src="/foo.js"></script>', $result);
    }

    public function testArrayScriptNoVersion()
    {
        $result = $this->helper->script(['foo.js', 'bar.js']);
        $this->assertNotEquals(
            false,
            strpos($result, '<script src="/foo.js"></script>')
        );
        $this->assertNotEquals(
            false,
            strpos($result, '<script src="/bar.js"></script>')
        );
    }

    public function testCacheHit()
    {
        Cache::write('static_version', 'somehashvalue', 'default');
        $hash = $this->helper->getStaticVersion();
        $result = $this->helper->script('foo.js', ['versioned' => true]);
        $this->assertEquals('<script src="/foo.js?hash=somehashvalue"></script>', $result);
    }
}
